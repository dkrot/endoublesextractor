import org.junit.*;
import static org.junit.Assert.*;


public class EnDoublesExtractorTest {
    @Test
    public void TestDbPath()
    {
        assertEquals(100003,
                EnDoublesExtractor.DoublesMapper.ExtractMicrobaseFromPath("/index/releases/r83/microbases/db_100003_20161002022124/data/url.data"));

        assertEquals(1,
                EnDoublesExtractor.DoublesMapper.ExtractMicrobaseFromPath("/index/releases/r83/microbases/db_1_20161002022124/data/url.data"));

        assertEquals(-1,
                EnDoublesExtractor.DoublesMapper.ExtractMicrobaseFromPath("/index/releases/r83/microbases"));
    }

    @Test
    public void TestRusEn()
    {
        assertTrue(EnDoublesExtractor.IsRussianBase(1));
        assertTrue(EnDoublesExtractor.IsRussianBase(500));
        assertTrue(EnDoublesExtractor.IsRussianBase(2999));

        assertFalse(EnDoublesExtractor.IsEnglishBase(1));
        assertFalse(EnDoublesExtractor.IsEnglishBase(500));
        assertFalse(EnDoublesExtractor.IsEnglishBase(2999));

        assertTrue(EnDoublesExtractor.IsEnglishBase(100001));
        assertTrue(EnDoublesExtractor.IsEnglishBase(100251));
        assertTrue(EnDoublesExtractor.IsEnglishBase(102999));

        assertFalse(EnDoublesExtractor.IsRussianBase(100001));
        assertFalse(EnDoublesExtractor.IsRussianBase(100251));
        assertFalse(EnDoublesExtractor.IsRussianBase(102999));
    }
}
