import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class EnDoublesExtractor extends Configured implements Tool {
    static final int RU_START_ID = 1;
    static final int RU_END_ID   = 2999;
    static final int EN_SHIFT_SIZE = 100000;

    static public boolean IsRussianBase(int id) {
        return id >= RU_START_ID && id <= RU_END_ID;
    }

    static public boolean IsEnglishBase(int id) {
        return id >= (EN_SHIFT_SIZE+RU_START_ID) && id <= (EN_SHIFT_SIZE+RU_END_ID);
    }


    public static class DoublesMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
        static Pattern dbid_pattern = Pattern.compile("db_(\\d+)_");
        IntWritable microbase_id;

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            String input_path = ((FileSplit)context.getInputSplit()).getPath().toString();
            int db_id = ExtractMicrobaseFromPath(input_path);
            if (db_id == -1 || !(IsRussianBase(db_id) || IsEnglishBase(db_id)))
                throw new InterruptedException(String.format("Bad database ID on input path (%s)", input_path));

            microbase_id = new IntWritable(db_id);
        }

        static int ExtractMicrobaseFromPath(String input) {
            Matcher matcher = dbid_pattern.matcher(input);
            return matcher.find() ? Integer.valueOf(matcher.group(1)) : -1;
        }

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String line = value.toString();
            String[] id_url = line.split("\t");

            if (id_url.length != 2) {
                context.getCounter("COMMON_COUNTERS", "BadRecords").increment(1);
                return;
            }

            context.write(new Text(id_url[1]), microbase_id);
        }
    }

    public static class DoublesPartitioner extends Partitioner<Text, IntWritable> {
        @Override
        public int getPartition(Text text, IntWritable db_id, int numPartitions) {
            int id = db_id.get();
            return (id <= RU_END_ID) ? id : id - EN_SHIFT_SIZE;
        }
    }

    public static class DoublesReducer extends Reducer<Text, IntWritable, Text, NullWritable> {
        @Override
        protected void reduce(Text url, Iterable<IntWritable> db_ids, Context context) throws IOException, InterruptedException {
            boolean found_russian = false;
            boolean found_english = false;

            for (IntWritable wid: db_ids) {
                int id = wid.get();
                found_russian |= IsRussianBase(id);
                found_english |= IsEnglishBase(id);
            }

            if (found_english && found_russian)
                context.write(url, NullWritable.get());
        }
    }

    public static Job GetJobConf(Configuration conf, String[] inputs, String out_dir) throws IOException {
        Job job = Job.getInstance(conf);
        job.setJarByClass(EnDoublesExtractor.class);
        job.setJobName(EnDoublesExtractor.class.getCanonicalName());

        for (String input: inputs) {
            FileInputFormat.addInputPath(job, new Path(input));
        }
        FileOutputFormat.setOutputPath(job, new Path(out_dir));

        job.setMapperClass(DoublesMapper.class);
        job.setPartitionerClass(DoublesPartitioner.class);
        job.setReducerClass(DoublesReducer.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        // print URLs which exists in RU and in paired EN microbases
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(NullWritable.class);

        // output of first reducer (id=0) will be empty
        // that's need to easy copying results to microbases
        job.setNumReduceTasks(2999 + 1);

        return job;
    }

    public static void main(String[] args) throws Exception {
        int rc = ToolRunner.run(new EnDoublesExtractor(), args);
        System.exit(rc);
    }

    @Override
    public int run(String[] args) throws Exception {
        String out_dir = args[args.length - 1];
        ArrayList<String> inputs = new ArrayList<String>();

        for (int i = 0; i < args.length-1; i++)
            inputs.add(args[i]);

        Job job = GetJobConf(getConf(), inputs.toArray(new String[0]), out_dir);
        return job.waitForCompletion(true) ? 0 : 1;
    }
}
